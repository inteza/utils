Pod::Spec.new do |s|

  s.name         = "Utils"
  s.version      = "0.0.5"
  s.summary      = "The set of useful utils and obj-c categories"



  s.homepage     = "http://inteza.net"

  s.license      = { :type => "MIT", :file => "LICENSE" }


  s.author             = { "Valentin Totsky" => "vtotskyi@gmail.com" }
  s.platform     = :ios, "7.0"

  s.source       = { :git => "https://bitbucket.org/inteza/utils", :tag =>  s.version.to_s }

  s.source_files  = "*.{h,m}"
  s.public_header_files = "*.h"
  s.requires_arc = true

  # s.xcconfig = { "HEADER_SEARCH_PATHS" => "$(SDKROOT)/usr/include/libxml2" }
  # s.dependency "JSONKit", "~> 1.4"

end
