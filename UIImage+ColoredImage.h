//
//  UIImage+ColoredImage.h
//  Bash Reader
//
//  Created by Тоцкий Валентин on 04.09.13.
//  Copyright (c) 2013 A2-Soft. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIImage (ColoredImage)

- (UIImage *)imageWithOverlayColor:(UIColor *)color;

@end
